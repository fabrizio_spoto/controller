#include <gtk/gtk.h>
#include <crypt.h>
#include <stdio.h>
#include <string.h>

#define PASS "echo 'raspberry' | sudo -S "

//per compilare:
// gcc -o login login.c $(pkg-config --libs --cflags gtk+-3.0) -lcrypt


static void dialog_error(void){
	
		GtkWidget   *dialog_error=gtk_message_dialog_new_with_markup(
												NULL, 
												GTK_DIALOG_MODAL,
												GTK_MESSAGE_ERROR,
												GTK_BUTTONS_OK,
												"credenziali errate, riprovare!");
		gtk_dialog_run(GTK_DIALOG(dialog_error));
		gtk_widget_destroy(dialog_error);
	
	
}


static void dialog_success(void){
	
		GtkWidget   *dialog_success=gtk_message_dialog_new_with_markup(
												NULL, 
												GTK_DIALOG_MODAL,
												GTK_MESSAGE_INFO,
												GTK_BUTTONS_OK,
												"credenziali giuste!");
		gtk_dialog_run(GTK_DIALOG(dialog_success));
		gtk_widget_destroy(dialog_success);
		char s[160];
		
		strcpy(s, PASS);
		strcat(s, "cp /home/pi/App/controller/no_comm.xml /home/pi/.config/openbox/ && sudo -S mv /home/pi/.config/openbox/no_comm.xml /home/pi/.config/openbox/lxde-pi-rc.xml");
		system(s);
		system("openbox --restart");
		system("xmodmap -e 'keycode 70 = F4 F4 F4 F4 F4 F4 XF86Switch_VT_4'");
		

		strcpy(s, PASS);
		strcat(s, "rm /etc/udev/rules.d/10-local.rules");
		system(s);
		
}

static void dialog_success_user(void){
	
		GtkWidget   *dialog_success=gtk_message_dialog_new_with_markup(
												NULL, 
												GTK_DIALOG_MODAL,
												GTK_MESSAGE_INFO,
												GTK_BUTTONS_OK,
												"credenziali giuste!\nPremi ok e attendi");
		gtk_dialog_run(GTK_DIALOG(dialog_success));
		printf("creo lista\n");
		
		char s[160];
		
		//creo lista
		strcpy(s, PASS);
		strcat(s, "/home/pi/App/controller/crea_lista");
		system(s);
		
		//caolcolo hash e salvataggio su file
		strcpy(s, PASS);
		strcat(s, "/home/pi/App/controller/hash_libreria_controller /home/pi/App/list_file.txt");
		system(s);
		
		//confronto con hash salvato su tpm
		strcpy(s, PASS);
		strcat(s,"/home/pi/App/controller/controllo_hash_libreria");
		system(s);
		
		//calcolo hash (file controllore) e salvataggio su file
		strcpy(s, PASS);
		strcat(s,"/home/pi/App/controller/hash_tpm_controller /home/pi/App/list_file_controller.txt");
		system(s);
		
		//confronto con hash salvato su tpm
		strcpy(s, PASS);
		strcat(s,"/home/pi/App/controller/controllo_hash_tpm");
		system(s);
		
		//elimina file contenenti l'hash
		strcpy(s, PASS);
		strcat(s,"rm /home/pi/App/list_file.txt");
		system(s);
		
		strcpy(s, PASS);
		strcat(s,"rm /home/pi/App/list_file_controller.txt");
		system(s);
		
		strcpy(s, PASS);
		strcat(s,"rm /home/pi/App/controller/*.txt");
		system(s);
		
		gtk_widget_destroy(dialog_success);
		
		//abilita shortcut tastiera
		strcpy(s, PASS);
		strcat(s,"cp /home/pi/App/controller/no_comm.xml /home/pi/.config/openbox/ && sudo -S mv /home/pi/.config/openbox/no_comm.xml /home/pi/.config/openbox/lxde-pi-rc.xml");
		system(s);
		system("openbox --restart");
		system("xmodmap -e 'keycode 70 = F4 F4 F4 F4 F4 F4 XF86Switch_VT_4'");
		
		//avvio in bg del processo che controlla periodicamente se la tpm è connessa e se è stato manomesso il controller
		strcpy(s, PASS);
		strcat(s,"bash /home/pi/App/controller/check_tpm.sh &");
		system(s);
		//system("bg");
		
}
static void
shutdown_function(GtkWidget *widget,
            GtkWidget *data)
{
	char s[160];
	strcpy(s, PASS);
	strcat(s, "shutdown now");
	system(s);
}

static void
login (GtkWidget *widget,
            GtkWidget *data)
{
	GtkWidget **w =(GtkWidget **) data;
	gchar *text_user=(gchar *)gtk_entry_get_text(GTK_ENTRY(w[0]));
	gchar *text_pass=(gchar *)gtk_entry_get_text(GTK_ENTRY(w[1]));
	
	//verifico se la tpm è collegata al raspberry
	FILE *fp;
	system("tpm_version > /tmp/tmp.txt");
	fp=fopen("/tmp/tmp.txt", "r");
	if(fp==NULL){
		 system("rm /tmp/tmp.txt");
		 system("shutdown now");
		 return;
	}
	
	fseek(fp,0,SEEK_END);
	int size=ftell(fp);
	if(0==size){
			system("rm /tmp/tmp.txt");
			system("shutdown now");
			return;
	}
	
	system("rm /tmp/tmp.txt");
	char s[160];
	strcpy(s, PASS);
	
	//verifico le credenziali dell'admin
	strcat(s, "tpm_nvread -l none -i 0x00000017 -pnv123 -n 48 -s 34 -f /home/pi/App/pass.txt");
	system(s);
	strcpy(s, PASS);
	strcat(s, "tpm_nvread -l none -i 0x00000017 -pnv123 -s 34 -f /home/pi/App/user.txt");
	system(s);
	
	strcpy(s, PASS);
	strcat(s, "chmod 444 /home/pi/App/pass.txt");
	system(s);
	
	strcpy(s, PASS);
	strcat(s, "chmod 444 /home/pi/App/user.txt");
	system(s);
	
	
	char user[35],password[35],*p,*u;
	int n=1,a,b;
	
	fp=fopen("/home/pi/App/pass.txt", "r");
	if(fp==NULL){
		printf ("errore apertura file\n");
		system("shutdown now");
		return;
	}
	fscanf(fp, "%s\0", password);
	fclose(fp);
	
	fp=fopen("/home/pi/App/user.txt", "r");
	if(fp==NULL){
		printf ("errore apertura file\n");
		system("shutdown now");
		return;
	}
	fscanf(fp, "%s\0", user);
	fclose(fp);
	strcpy(s, PASS);
	strcat(s, "rm /home/pi/App/pass.txt && sudo -S rm /home/pi/App/user.txt");
	system(s);
	system("clear");
	char salt[11];
	b=0;
	while(b<11){
		salt[b]=user[b];
		b++;
	}
		
	u=crypt((char *)text_user, salt);
	a=strcmp(u,user);
	b=0;
	while(b<11){
		salt[b]=password[b];
		b++;
	}
	
	p=crypt((char *)text_pass, salt);
	b=strcmp(p,password);
	
	if(a==0 && b==0)
	{
		dialog_success();
	}
	else //verifico le credenziali dell'user
	{
		u[0]='\0';
		p[0]='\0';
		
		strcpy(s, PASS);
		strcat(s, "tpm_nvread -l none -i 0x00000117 -pnvuser -n 48 -s 34 -f /home/pi/App/pass.txt");
		system(s);
		strcpy(s, PASS);
		strcat(s, "tpm_nvread -l none -i 0x00000117 -pnvuser -s 34 -f /home/pi/App/user.txt");
		system(s);
		
		strcpy(s, PASS);
		strcat(s, "chmod 444 /home/pi/App/pass.txt");
		system(s);
		
		strcpy(s, PASS);
		strcat(s, "chmod 444 /home/pi/App/user.txt");
		system(s);
			
	
		fp=fopen("/home/pi/App/pass.txt", "r");
		if(fp==NULL){
			printf ("errore apertura file\n");
			system("shutdown now");
			return;
		}
		fscanf(fp, "%s\0", password);
		fclose(fp);
		
		fp=fopen("/home/pi/App/user.txt", "r");
		if(fp==NULL){
			printf ("errore apertura file\n");
			system("shutdown now");
			return;
		}
		fscanf(fp, "%s\0", user);
		fclose(fp);
		strcpy(s, PASS);
		strcat(s, "rm /home/pi/App/pass.txt && sudo -S rm /home/pi/App/user.txt");
		system(s);
		system("clear");
		
		char salt2[11];
		b=0;
		while(b<11){
			salt2[b]=user[b];
			b++;
		}
		
		u=crypt((char *)text_user, salt2);
		a=strcmp(u,user);
		
		b=0;
		while(b<11){
			salt2[b]=password[b];
			b++;
		}
		p=crypt((char *)text_pass, salt2);
		b=strcmp(p,password);
		
		if(a==0 && b==0)
		{
			dialog_success_user();	
		}
		else{
			dialog_error();
			char s[160];
			strcpy(s, PASS);
			strcat(s, "reboot ");
			system(s);
		}
	}
	
	
	
	
}



static void
activate (GtkApplication *app,
          gpointer        user_data)
{
	GtkWidget *window;
	GtkWidget *button,*exit_button;
	GtkWidget *button_box;

	window = gtk_application_window_new (app);
	gtk_window_set_title (GTK_WINDOW (window), "Window");
	gtk_window_fullscreen(GTK_WINDOW(window));
	button_box = gtk_box_new (GTK_ORIENTATION_VERTICAL,50);
	gtk_container_add (GTK_CONTAINER (window), button_box);
	gtk_window_set_deletable(GTK_WINDOW(window),FALSE);


	GtkWidget *label1,*label2;
	label1=gtk_label_new("USERNAME");
	label2=gtk_label_new("PASSWORD");

	GtkWidget *username_entry, *password_entry;
	username_entry = gtk_entry_new();
	password_entry = gtk_entry_new();
	gtk_entry_set_visibility(GTK_ENTRY(password_entry), FALSE);
	
	
	gtk_container_add (GTK_CONTAINER (button_box), label1);
	gtk_box_pack_start(GTK_BOX(button_box), username_entry, FALSE, FALSE, 0);
	gtk_container_add (GTK_CONTAINER (button_box), label2);
	gtk_box_pack_start(GTK_BOX(button_box), password_entry, FALSE, FALSE, 0);
	
	GtkWidget **temp;
	temp=g_new(GtkWidget *, 2);
	temp[0]=username_entry;
	temp[1]=password_entry;
	
	
	
	button = gtk_button_new_with_label ("Login");
	exit_button = gtk_button_new_with_label ("exit");

	g_signal_connect (button, "clicked", G_CALLBACK(login), temp );
	g_signal_connect_swapped (button, "clicked", G_CALLBACK (gtk_widget_destroy), window);
	
	g_signal_connect_swapped (exit_button, "clicked", G_CALLBACK (shutdown_function), window);
	
	gtk_container_add (GTK_CONTAINER (button_box), button);
	gtk_container_add (GTK_CONTAINER (button_box), exit_button);
	
	
	char s[160];
	
	 //disabilito momentaneamente le shortcut della tastiera 
    strcpy(s, PASS);
    strcat(s, "cp /home/pi/App/controller/si_comm.xml /home/pi/.config/openbox/ && sudo -S mv /home/pi/.config/openbox/si_comm.xml /home/pi/.config/openbox/lxde-pi-rc.xml");
    system(s);
    system("openbox --restart");
	  
	 
	system("xmodmap -e 'keycode 70 = '");
	
	//elimino vecchi file se presenti
	strcpy(s, PASS);
	strcat(s, "rm /home/pi/App/list_file.txt");
	system(s);
	
	strcpy(s, PASS);
	strcat(s, "rm /home/pi/App/list_file_controller.txt");
	system(s);
	
	//verifico se la tpm è collegata al raspberry
	FILE *fp;
	system("tpm_version > /tmp/tmp.txt");
	fp=fopen("/tmp/tmp.txt", "r");
	if(fp==NULL){
		 system("rm /tmp/tmp.txt");
		 system("shutdown now");
		 return;
	}
	
	fseek(fp,0,SEEK_END);
	int size=ftell(fp);
	if(0==size){
			system("rm /tmp/tmp.txt");
			system("shutdown now");
			return;
	}
	
	system("rm /tmp/tmp.txt");
	
	gtk_widget_show_all (window);
}

int
main (int    argc,
      char **argv)
{
  char s[160];
  
 
  //disabilito momentaneamente il filtro anti-tampering
  strcpy(s, PASS);
  strcat(s, "cp /home/pi/App/controller/10-local.rules /etc/udev/rules.d/");
  system(s);
  
  //abilito la tpm
  strcpy(s, PASS);
  strcat(s, "pkill -x tcsd");
  system(s);
  strcpy(s, PASS);
  strcat(s, "python /home/pi/App/controller/tpmstartup.py -a");
  system(s);
  strcpy(s, PASS);
  strcat(s, "tcsd");
  system(s);


  //aggiornamento hash sulla tpm
  strcpy(s, PASS);
  strcat(s, "tpm_nvwrite -i 0x000217 -pnvhash -f /home/pi/App/hash_libreria.txt");
  system(s);
  
  strcpy(s, PASS);
  strcat(s, "rm /home/pi/App/hash_libreria.txt");
  system(s);
  
  
  GtkApplication *app;
  int status;

  app = gtk_application_new (NULL, G_APPLICATION_FLAGS_NONE);
  
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}
