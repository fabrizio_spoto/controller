#!/bin/bash
# check_tpm.sh

trap "echo 'raspberry' | sudo -S ~/controller/shutdown_controller" INT TERM PWR STOP KILL

while :
do 
	#controllo se la tpm è collegata
	tpm_version > /dev/null || shutdown now	
	sleep 5
	
	#controllo hash sui file del controllore
	echo 'raspberry' | sudo -S find ~ -xdev -type f> ~/list_file_controller.txt
	echo 'raspberry' | sudo -S sed -i '/.txt/d' ~list_file_controller.txt
	echo 'raspberry' | sudo -S ~/hash_tpm_controller ~/list_file_controller.txt
	echo 'raspberry' | sudo -S ~/controllo_hash_tpm
	
	#cancello file temporanei
    echo 'raspberry' | sudo -S rm ~/list_file.txt
	echo 'raspberry' | sudo -S rm ~/list_file_controller.txt
	echo 'raspberry' | sudo -S rm ~/*.txt
	 
		

done
