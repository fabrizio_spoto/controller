# Framework Controller

La repository contiene il codice per controllo dello stato della piattaforma Raspberry pi 2 Model b.

Descrizione file

-login.c: interfaccia grafica, tramite libreria gtk, di login; effettua il controllo delle credenziali, se sbagliate il sitema viene spento
		  altrimenti viene fatto un calcolo dell'hash del sistema per verificare se sono state effettuate manomissioni da parte di utenti non autorizzati
		  
-check-tpm.sh: script che effettua priodicamente 2 controlli
				1.verifica la presenza della TPM, altrimenti il sistema viene spento
				2.verifica se sono stati modificati file da utenti non autorizzati

-anti-tampering.rules: whitelist sui dispositi usb che possono essere collegati; altri dispositivi non verranno accettati;
					   controllo sulla rimozione dei dispositivi in whitelist
					   
-tpmstartup.py: script per abilitare la TPM dopo l'avvio della piattaforma
	

